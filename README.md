# Worlds Of Ursula K Le Guin ESP
## PADs de trabajo

[**Traducción en curso**](https://pad.riseup.net/p/UrsulaK_NzEvOS_ZLOoY1fgQDZJ1-keep) | [**Español de referencia**](https://pad.riseup.net/p/arX98XpaVNl52N6NlRZx-keep)


## Subtítulos
Subtítulos en español para de Worlds Of Ursula K. Le Guin de la versión 720p WEBRip de YTS.MX

[**Sub Español**](https://gitlab.com/krv00/worlds-of-ursula-k-le-guin-esp/-/raw/main/srt/Worlds.Of.Ursula.K..Le.Guin.2018.720p.WEBRip.x264.AAC-%5BYTS.MX%5D-SPA.srt?inline=false) | [**Sub Ingles**]( https://gitlab.com/krv00/worlds-of-ursula-k-le-guin-esp/-/raw/main/srt/Worlds.Of.Ursula.K..Le.Guin.2018.720p.WEBRip.x264.AAC-%5BYTS.MX%5D-ENG.srt?inline=false)

## Documental
**Nombre:** Worlds Of Ursula K. Le Guin

**Director:** Arwen Curry

**Año:** 2018

![portada](https://gitlab.com/krv00/worlds-of-ursula-k-le-guin-esp/-/raw/main/medium-cover.jpg)

## Descarga Documental YTS

**Página**:
https://yts.mx/movies/worlds-of-ursula-k-le-guin-2018

**Magnet 720**:
https://yts.mx/torrent/download/E8DF2E3CE115633F6A38DFE552456810FCDDA853

**Magnet 1080**:
https://yts.mx/torrent/download/B54D17BDD997D5B1D464DF8FE1531BA0F767C22E